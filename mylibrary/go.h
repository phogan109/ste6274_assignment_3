#ifndef GO_H
#define GO_H



//stl
#include <map>
#include <memory>
#include <utility>
#include <chrono>


#include <set>
#include <vector>




namespace mylib {

namespace go {



  class Board;
  class Stone;
  class Block;
  class Boundary;
  struct GameState;


  class Player;
  class AiPlayer;
  class HumanPlayer;

  class Engine;









  using time_type = std::chrono::duration<int,std::milli>;

  constexpr time_type operator"" _ms (unsigned long long int milli) { return time_type(milli); }
  constexpr time_type operator"" _s  (unsigned long long int sec)   { return time_type(sec*1000); }








  enum class StoneColor {
    White       = 0,
    Black       = 1
  };


  enum class PlayerType {
    Human       = 0,
    Ai          = 1
  };

  enum class GameMode {
    VsPlayer    = 0,
    VsAi        = 1,
    Ai          = 2
  };

  using Point   = std::pair<int,int>;
  using Size    = std::pair<int,int>;















  namespace priv {


    class Stone_base {

        public:
              StoneColor _color;
              Point _point;
              std::shared_ptr<Board> _board;

              Stone_base() = default;
              explicit Stone_base(Point _point, StoneColor _color, std::shared_ptr<Board> _board);

              bool hasNorth() const;
              bool hasEast() const;
              bool hasSouth() const;
              bool hasWest() const;

              Point north() const;
              Point east() const;
              Point south() const;
              Point west() const;

              StoneColor color() const;
              Point point() const;
              Board board() const;


      };

    class Boundary_base {

       public:
              std::vector<Point> _points;


              Boundary_base() = default;
              explicit Boundary_base(std::vector<Point> _points);

    };



    class Block_base {

        public:
            std::set<Point> _block;
            std::set<std::shared_ptr<Boundary>> _boundaries;
            std::shared_ptr<Board> _board;
            StoneColor _color;

            Block_base() = default;
            explicit Block_base(std::set<Point> _block, std::shared_ptr<Board> _board, StoneColor _color);

            std::set<Point> block() const;
            Board board() const;
            StoneColor color() const;


    };


    // INVARIANTS:
    //   Board fullfills; what does one need to know in order to consider a given position.
    //   * all stones and their position on the board
    //   * who places the next stone
    //   * was last move a pass move
    //   * meta: blocks and freedoms


    class Board_base {
    public:
      using BoardData = std::map<Point,Stone>;
      using BlockData = std::set<std::shared_ptr<Block>>;

      struct Position {
        BoardData     board;
        StoneColor    turn;
        bool          was_previous_pass;
        BlockData blocks;

        Position () = default;
        explicit Position ( BoardData&& data, StoneColor turn, bool was_previous_pass);

      }; // END class Position


      // Constructors
      Board_base() = default;
      explicit Board_base( Size size );
      explicit Board_base( BoardData&& data, StoneColor turn, bool was_previous_pass );



      // Board data
      Size             size() const;
      bool             wasPreviousPass() const;
      StoneColor            turn() const;


      // Board
      void             resetBoard( Size size );


      // Board
      Size             _size;
      Position         _current;


    }; // END base class Board_base



  } // END "private" namespace priv


  class Stone : private priv::Stone_base {

  public:
      using Stone_base::Stone_base;

      using Stone_base::_color;
      using Stone_base::_point;
      using Stone_base::_board;

      using Stone_base::hasNorth;
      using Stone_base::hasEast;
      using Stone_base::hasSouth;
      using Stone_base::hasWest;
      using Stone_base::north;
      using Stone_base::east;
      using Stone_base::south;
      using Stone_base::west;

      using Stone_base::color;
      using Stone_base::point;
      using Stone_base::board;


  };

  class Boundary : private priv::Boundary_base {

  public:

      using Boundary_base::Boundary_base;

      using Boundary_base::_points;

  };



  class Block : private priv::Block_base {

  public:
      using Block_base::Block_base;

      using Block_base::_block;
      //using Block_base::_boundaries;
      using Block_base::_board;
      using Block_base::_color;

      using Block_base::block;
      using Block_base::board;
      using Block_base::color;

};





  class Board : private priv::Board_base {
  public:
    // Enable types
    using Board_base::BoardData;
    using Board_base::BlockData;
    using Board_base::Position;

    // Enable constructors
    using Board_base::Board_base;

    // Make visible from Board_base
    using Board_base::resetBoard;
    using Board_base::size;
    using Board_base::wasPreviousPass;
    using Board_base::turn;


    // Validate
    bool                  isNextPositionValid( Point intersection ) const;
    bool                  isOutOfBounds( Point intersection ) const;

    // Actions
    void                  placeStone( Point intersection );
    void                  passTurn();
    void                  maintainBlocks( Point intersection );
    void                  maintainBoundaries( Point intersection, Block block );

    // Stones and positions
    bool                  hasStone( Point intersection ) const;
    Stone                 stone( Point intersection ) const;
    std::set<Point>       adjacentSameColor( Stone intersection ) const;
    std::set<Point>       adjacentDiffColor( Stone intersection ) const;
    std::set<Point>       adjacentNoStone( Stone intersection ) const;
    std::set<Point>       adjacentDiffColorNoStone( Stone intersection ) const;


  }; // END class Board




















  class Engine : public std::enable_shared_from_this<Engine> {
  public:
    Engine();

    void                            newGame( Size size );
    void                            newGameVsAi( Size size );
    void                            newGameAiVsAi( Size size );
    void                            newGameFromState( Board::BoardData&& board, StoneColor turn,
                                                      bool was_previous_pass );

    const Board&                    board() const;

    StoneColor                           turn() const;

    const GameMode&                 gameMode() const;
    const std::shared_ptr<Player>   currentPlayer() const;

    void                            placeStone( Point intersection );
    bool                            validateStone( Point intersection ) const;
    void                            passTurn();

    void                            nextTurn( time_type think_time = 100_ms );
    bool                            isGameActive() const;

    void                            maintainBlocks( Point intersection ) const;
    void                            maintainBoundaries( Point intersection ) const;

  private:
    Board                           _board;

    GameMode                        _game_mode;
    bool                            _active_game;

    std::shared_ptr<Player>         _white_player;
    std::shared_ptr<Player>         _black_player;

  }; // END class Engine


































} // END namespace go

} // END namespace mylib

#endif //GO_H
