#include "go.h"
#include "randomai.h"
#include <algorithm>
#include <iostream>


namespace mylib {
namespace go {



  namespace priv {


  /**********************************************BOARD_BASE******************************************************/


    Board_base::Board_base( Size size ) {

      resetBoard(size);
    }

    Board_base::Board_base(Board::BoardData&& data, StoneColor turn, bool was_previous_pass)
      : _current{std::forward<Board::BoardData>(data),turn,was_previous_pass}
    {
      // ... init ...
    }

    Board_base::Position::Position(Board::BoardData&& data, StoneColor trn, bool prev_pass)
      : board{data}, turn{trn}, was_previous_pass{prev_pass}, blocks{} {}


    void
    Board_base::resetBoard(Size size) {

      _current.board.clear();
      _size = size;
      _current.turn = StoneColor::Black;
      _current.blocks.clear();
    }

    Size
    Board_base::size() const {

      return _size;
    }

    bool
    Board_base::wasPreviousPass() const {

      return _current.was_previous_pass;
    }

    StoneColor
    Board_base::turn() const {

      return _current.turn == StoneColor::Black ? StoneColor::White : StoneColor::Black;
    }

    /**********************************************STONE_BASE******************************************************/

    Stone_base::Stone_base(Point _point, StoneColor _color, std::shared_ptr<Board> _board)
    {
        this->_point = _point;
        this->_color = _color;
        this->_board = _board;
    }

    bool
    Stone_base::hasNorth() const
    {
        if ( !(*(this->_board)).isOutOfBounds( this->north() ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool
    Stone_base::hasEast() const
    {
       return (*(this->_board)).hasStone(this->east());
    }

    bool
    Stone_base::hasSouth() const
    {
       return (*(this->_board)).hasStone(this->south());
    }

    bool
    Stone_base::hasWest() const
    {
       return (*(this->_board)).hasStone(this->west());
    }



    Point
    Stone_base::north() const
    {
        return Point(_point.first, _point.second-1);
    }

    Point
    Stone_base::east() const
    {
        return Point(_point.first+1, _point.second);
    }

    Point
    Stone_base::south() const
    {
        return Point(_point.first, _point.second+1);
    }

    Point
    Stone_base::west() const
    {
        return Point(_point.first-1, _point.second);
    }

    StoneColor
    Stone_base::color() const
    {
        return this->_color;
    }

    Point
    Stone_base::point() const
    {
        return this->_point;
    }

    Board
    Stone_base::board() const
    {
        return *(this->_board);
    }






   /**********************************************BOUNDARY_BASE******************************************************/

    Boundary_base::Boundary_base(std::vector<Point> _points)
    {
        this->_points = _points;
    }


    /**********************************************BLOCK_BASE******************************************************/

     Block_base::Block_base(std::set<Point> _block, std::shared_ptr<Board> _board, StoneColor _color)
     {
         this->_block = _block;
         this->_board = _board;
         this->_color = _color;
     }

     StoneColor
     Block_base::color() const
     {
         return this->_color;
     }

     Board
     Block_base::board() const
     {
         return *(this->_board);
     }

     std::set<Point>
     Block_base::block() const
     {
         return this->_block;
     }

  }

  /**********************************************BOARD******************************************************/

  void
  Board::placeStone(Point intersection) {

    _current.board[intersection] = Stone {intersection, _current.turn, std::shared_ptr<Board>(this)};
    _current.turn = turn();
  }

  void
  Board::passTurn() {

    _current.turn = turn();
  }

  void
  Board::maintainBlocks( Point intersection ) {

      std::set<Point> newBlockPoints = {intersection};


      for (auto h = this->adjacentSameColor(this->stone(intersection)).begin(); h != adjacentSameColor(this->stone(intersection)).end(); h++)
      {
                for (auto i = _current.blocks.begin(); i != _current.blocks.end(); i++)
                {
                    auto j = (**i)._block.find( *h );

                    if ( j != (**i)._block.end() )
                    {
                        newBlockPoints.insert((**i)._block.begin(), (**i)._block.end());
                        _current.blocks.erase(*i);
                        _current.blocks.erase(i);
                    }
                }
      }

      Block newBlock = Block { newBlockPoints, std::shared_ptr<Board>(this), this->stone(intersection).color() };

      _current.blocks.insert( std::make_shared<Block>( newBlock ) );

      this->maintainBoundaries(intersection, newBlock);

  }

  void
  Board::maintainBoundaries( Point intersection, Block block )  {

        for (auto i = block.block().begin(); i != block.block().end(); i++)
        {

        }

  }

  bool
  Board::hasStone(Point intersection) const {

      if (intersection.first <= this->_size.first && intersection.second <= this->_size.second)
      {
          return _current.board.count(intersection);
      }
      else
      {
          return false;
      }
  }

  Stone
  Board::stone(Point intersection) const {

      if (this->hasStone(intersection))
      {
          return _current.board.at(intersection);
      }
      else
      {
          throw;
      }
  }

  std::set<Point>
  Board::adjacentSameColor( Stone intersection ) const {

      std::set<Point> returnSet;

      if ( !this->isOutOfBounds(intersection.north() ) )
      {
          if ( this->hasStone( intersection.north() ) )
          {
              if ( intersection.color() == this->stone( intersection.north() ).color() )
              {
                  returnSet.insert(intersection.north());
              }
          }
      }

      if ( !this->isOutOfBounds(intersection.east()))
      {
          if ( this->hasStone( intersection.east() ) )
          {
              if ( intersection.color() == this->stone( intersection.east() ).color() )
              {
                  returnSet.insert(intersection.east());
              }
          }
      }

      if ( !this->isOutOfBounds( intersection.south() ) )
      {
          if ( this->hasStone( intersection.south() ) )
          {
              if ( intersection.color() == this->stone( intersection.south() ).color() )
              {
                  returnSet.insert(intersection.south());
              }
          }
      }

      if ( !this->isOutOfBounds(intersection.west() ) )
      {
          if ( this->hasStone( intersection.west() ) )
          {
              if ( intersection.color() == this->stone( intersection.west() ).color() )
              {
                  returnSet.insert(intersection.west());
              }
          }
      }

      return returnSet;

  }

  std::set<Point>
  Board::adjacentDiffColor( Stone intersection ) const {

      std::set<Point> returnSet;

      if ( !this->isOutOfBounds(intersection.north() ) )
      {
          if ( this->hasStone( intersection.north() ) )
          {
              if ( intersection.color() != this->stone( intersection.north() ).color() )
              {
                  returnSet.insert(intersection.north());
              }
          }
      }

      if ( !this->isOutOfBounds(intersection.east()))
      {
          if ( this->hasStone( intersection.east() ) )
          {
              if ( intersection.color() != this->stone( intersection.east() ).color() )
              {
                  returnSet.insert(intersection.east());
              }
          }
      }

      if ( !this->isOutOfBounds( intersection.south() ) )
      {
          if ( this->hasStone( intersection.south() ) )
          {
              if ( intersection.color() != this->stone( intersection.south() ).color() )
              {
                  returnSet.insert(intersection.south());
              }
          }
      }

      if ( !this->isOutOfBounds(intersection.west() ) )
      {
          if ( this->hasStone( intersection.west() ) )
          {
              if ( intersection.color() != this->stone( intersection.west() ).color() )
              {
                  returnSet.insert(intersection.west());
              }
          }
      }

      return returnSet;

  }

  std::set<Point>
  Board::adjacentNoStone( Stone intersection ) const {

      std::set<Point> returnSet;

      if ( !this->isOutOfBounds(intersection.north() ) )
      {
          if ( !this->hasStone( intersection.north() ) )
          {
             returnSet.insert(intersection.north());
          }
      }

      if ( !this->isOutOfBounds(intersection.east()))
      {
          if (! this->hasStone( intersection.east() ) )
          {
             returnSet.insert(intersection.east());
          }
      }

      if ( !this->isOutOfBounds( intersection.south() ) )
      {
          if ( this->hasStone( intersection.south() ) )
          {
             returnSet.insert(intersection.south());
          }
      }

      if ( !this->isOutOfBounds(intersection.west() ) )
      {
          if ( this->hasStone( intersection.west() ) )
          {
             returnSet.insert(intersection.west());
          }
      }

      return returnSet;

  }

  std::set<Point>
  Board::adjacentDiffColorNoStone( Stone intersection ) const {

      std::set<Point> returnSet;

      if ( !this->isOutOfBounds(intersection.north() ) )
      {
          if ( !this->hasStone( intersection.north() ) || ( intersection.color() != this->stone( intersection.north() ).color() ) )
          {
             returnSet.insert(intersection.north());
          }
      }

      if ( !this->isOutOfBounds(intersection.east()))
      {
          if (! this->hasStone( intersection.east() ) || ( intersection.color() != this->stone( intersection.north() ).color() ) )
          {
             returnSet.insert(intersection.east());
          }
      }

      if ( !this->isOutOfBounds( intersection.south() ) || ( intersection.color() != this->stone( intersection.north() ).color() ) )
      {
          if ( this->hasStone( intersection.south() ) )
          {
             returnSet.insert(intersection.south());
          }
      }

      if ( !this->isOutOfBounds(intersection.west() ) || ( intersection.color() != this->stone( intersection.north() ).color() ) )
      {
          if ( this->hasStone( intersection.west() ) )
          {
             returnSet.insert(intersection.west());
          }
      }

      return returnSet;

  }


  bool
  Board::isNextPositionValid(Point intersection) const {

    return true; // !this->hasStone(intersection);
  }

  bool
  Board::isOutOfBounds(Point intersection) const
  {
      if ( intersection.first > this->size().first || intersection.second > this->size().second )
      {
          return true;
      }
      else
      {
          return false;
      }
  }

  /**********************************************ENGINE*******************************************/

  Engine::Engine()
    : _board{}, _game_mode{}, _active_game{false},
      _white_player{nullptr}, _black_player{nullptr} {}

  void
  Engine::newGame(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameVsAi(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::VsAi;
    _active_game = true;

    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),   StoneColor::White);
  }

  void
  Engine::newGameAiVsAi(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::Ai;
    _active_game = true;

    _white_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameFromState(Board::BoardData&& board, StoneColor turn, bool was_previous_pass) {

    _board = Board {std::forward<Board::BoardData>(board),turn,was_previous_pass};

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  const Board&
  Engine::board() const {

    return _board;
  }

  const GameMode&
  Engine::gameMode() const {

    return _game_mode;
  }

  StoneColor
  Engine::turn() const {

    return _board.turn();
  }

  const std::shared_ptr<Player>
  Engine::currentPlayer() const {

    if(      turn() == StoneColor::Black ) return _black_player;
    else if( turn() == StoneColor::White ) return _white_player;
    else                              return nullptr;
  }

  void
  Engine::placeStone(Point intersection) {

    _board.placeStone(intersection);
  }

  void
  Engine::passTurn() {

    if(board().wasPreviousPass()) {
      _active_game = false;
      return;
    }

    _board.passTurn();
  }

  void
  Engine::nextTurn(std::chrono::duration<int,std::milli> think_time) {

    if( currentPlayer()->type() != PlayerType::Ai) return;

    auto p = std::static_pointer_cast<AiPlayer>(currentPlayer());

    p->think( think_time );
    if( p->nextMove() == AiPlayer::Move::PlaceStone )
      placeStone( p->nextStone() );
    else
      passTurn();
  }

  bool
  Engine::isGameActive() const {

    return _active_game;
  }

  bool
  Engine::validateStone(Point pos) const {

    return _board.isNextPositionValid(pos);
  }






} // END namespace go
} // END namespace mylib
