// My Go Engine
#include "../../mylibrary/go.h"

// gtest
#include <gtest/gtest.h>

#include <exception>



namespace go = mylib::go;

TEST(StoneTest,FromStateConstructor) {

    go::Stone _stone = go::Stone{go::Point{1,1}, go::StoneColor::Black, std::shared_ptr<go::Board>(nullptr)};


    //EXPECT the stone's point to be {1,1}
    EXPECT_TRUE(_stone._point == (go::Point{1,1}));

    //EXPECT the stone to be black
    EXPECT_TRUE(_stone._color == go::StoneColor::Black);

    //EXPECT the stone to have a null board pointer
    EXPECT_TRUE(_stone._board == nullptr);

    //EXPECT the stone's point to be {1,1}
    EXPECT_FALSE(_stone._point == (go::Point{2,2}));

    //EXPECT the stone to be black
    EXPECT_FALSE(_stone._color == go::StoneColor::White);

    //EXPECT the stone to have a null board pointer
    EXPECT_FALSE(_stone._board != nullptr);

}

TEST(StoneTest,GetDirections) {

    go::Stone _stone = go::Stone{go::Point{3,3}, go::StoneColor::Black, std::shared_ptr<go::Board>(nullptr)};


    //EXPECT the north point to be {3,2}
    EXPECT_TRUE(_stone.north() == (go::Point{2,3}));

    //EXPECT the east point to be {1,1}
    EXPECT_TRUE(_stone.east() == (go::Point{3,4}));

    //EXPECT the south point to be {1,1}
    EXPECT_TRUE(_stone.south() == (go::Point{4,3}));

    //EXPECT the west point to be {1,1}
    EXPECT_TRUE(_stone.west() == (go::Point{3,2}));



}

TEST(StoneTest,HasDirections) {

    /*

    go::Stone _stone = go::Stone{go::Point{3,3}, go::StoneColor::Black, std::shared_ptr<go::Board>(nullptr)};


    //EXPECT the north point to be {3,2}
    EXPECT_TRUE(_stone.hasNorth() == false);

    //EXPECT the east point to be {1,1}
    EXPECT_TRUE(_stone.hasEast() == false);

    //EXPECT the south point to be {1,1}
    EXPECT_TRUE(_stone.hasSouth() == false);

    //EXPECT the west point to be {1,1}
    EXPECT_TRUE(_stone.hasWest() == false);

    */


}

TEST(Board_Stone_Test, FromStateConstructor) {

    go::Board::BoardData board_data;


    // Create a engine from state
    go::Board board {std::move(board_data),go::StoneColor::Black,false};

    board.resetBoard(go::Point{5,5});

    board.placeStone(go::Point{2,3});

    EXPECT_TRUE(board.hasStone(go::Point{2,3}));

    EXPECT_TRUE(board.stone(go::Point{2,3})._color == go::StoneColor::Black);

}


TEST(Board_Stone_Test, MakingBlocks) {



}

TEST(BoundaryTest, FromStateConstructor) {

}

TEST(BlockTest, FromStateConstructor) {

}


